#!/bin/bash

sudo yum update -y
sudo amazon-linux-extras install -y docker
sudo systemctl enable docker.service
sudo systemctl start docker.service
# this is the default username from the aws linux ami that we will add to the docker group
sudo usermod -aG docker ec2-user