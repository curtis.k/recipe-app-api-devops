# use variables for defining static variables
variable "prefix" {
  default     = "raad"
  description = "prefix for recipe api app devops"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "curtis.k@hotmail.com"
}

variable "db_username" {
  description = "Username fo rthe RDS postgres instance"
}

variable "db_password" {
  description = "Password for the the RDS postgres instance"
}

variable "bastion_key_name" {
  description = "Key from key pairs we defined in the AWS bastion instnace"
  default     = "recipe-app-api-devops-bastion"
}
variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "366383880455.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-devops:latest"
}
variable "ecr_image_proxy" {
  description = "ECR image for proxy"
  default     = "366383880455.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-proxy:latest"
}
variable "django_secret_key" {
  description = "Secret key for Django app"
}

variable "dns_zone_name" {
  description = "Domain name for App"
  default     = "ryaski.com"
}

variable "subdomain" {
  description = "Subdomain per environment"
  type        = map(string)
  default = {
    production = "api"
    staging    = "api.staging"
    dev        = "api.dev"
  }
}
