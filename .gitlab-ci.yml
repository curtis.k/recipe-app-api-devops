#this image will get reused for most of our builds
image:
    name: hashicorp/terraform:0.12.21
    # this is needed to be able to run terraform commands from within the pipeline
    entrypoint:
        - '/usr/bin/env'
        - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'
    

stages:
    - Test and Lint
    - Build and Push
    - Staging Plan
    - Staging Apply
    - Production Plan
    - Production Apply
    - Destroy

# During the test and lint stage we are running multiple jobs, Test and Lint, Validate Terraform
Test and Lint:
    image: docker:19.03.5
    services:
        - docker:19.03.5-dind
    stage: Test and Lint
    script:
        - apk add python3-dev libffi-dev openssl-dev gcc libc-dev make
        - pip3 install -I docker-compose==1.26.2
        - docker-compose run --rm app sh -c "python manage.py wait_for_db && python manage.py test && flake8"
    rules:
        # this will be run twice, once when the merge request is made and then once when it is actually mereged into the master/production branches 
        - if: '$CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /^(master|production)$/ || $CI_COMMIT_BRANCH =~ /^(master|production)$/'

Validate Terraform:
    stage: Test and Lint
    script:
        - cd deploy/
        # since we arent actually applying anything here, we dont need to create the actual backend on S3, just apply it locally
        - terraform init -backend=false
        - terraform validate
        # the -check flag is just to check the formatting, not actually change the code
        - terraform fmt -check
    rules:
        # this will be run twice, once when the merge request is made and then once when it is actually mereged into the master/production branches 
        # this is because we want to test the formatting for the current branch but also make sure that changes already pushed to master dont affect this
        - if: '$CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /^(master|production)$/ || $CI_COMMIT_BRANCH =~ /^(master|production)$/'

Build and Push:
    image: docker:19.03.5
    services:
        - docker:19.03.5-dind
    stage: Build and Push
    script:
        - apk add python3
        - pip3 install awscli
        # we want to make sure that each tag is unique and this SHA will be unique for each commit
        # this will build the docker image as defined in the dockerfile in the root of our project
        - docker build --compress -t $ECR_REPO:$CI_COMMIT_SHORT_SHA .
        # gets and executes the docker login command
        - $(aws ecr get-login --no-include-email --region us-east-1)
        - docker push $ECR_REPO:$CI_COMMIT_SHORT_SHA
        # ensure to aslo tag with latest so we know which is the most recent
        - docker tag $ECR_REPO:$CI_COMMIT_SHORT_SHA $ECR_REPO:latest
        - docker push $ECR_REPO:latest
    rules:
        - if: '$CI_COMMIT_BRANCH =~ /^(master|production)$/'

# The plan doesnt actually do anything, aside from creating the workspace and outputting information
# this is broken up between plan and apply incase you wanted to run the apply job manually after reviewing the plan first
Staging Plan:
    stage: Staging Plan
    script:
        - cd deploy/
        # this defines an environment variable that can be used as a variable inside the terraform config
        - export TF_VAR_ecr_image_api=$ECR_REPO:$CI_COMMIT_SHORT_SHA
        - terraform init
        # try to select the staging workspace but if it doesnt exist then create it first
        - terraform workspace select staging || terraform workspace new staging
        - terraform plan
    rules:
        - if: '$CI_COMMIT_BRANCH =~ /^(master|production)$/'
    
Staging Apply:
    stage: Staging Apply
    script:
        - cd deploy/
        - export TF_VAR_ecr_image_api=$ECR_REPO:$CI_COMMIT_SHORT_SHA
        - terraform init
        # this runs after the staging plan, so we know the workspace will already exist
        - terraform workspace select staging
        - terraform apply -auto-approve
    rules:
        - if: '$CI_COMMIT_BRANCH =~ /^(master|production)$/'
            
Production Plan:
    stage: Production Plan
    script:
        - cd deploy/
        # this defines an environment variable that can be used as a variable inside the terraform config
        - export TF_VAR_ecr_image_api=$ECR_REPO:$CI_COMMIT_SHORT_SHA
        - terraform init
        # try to select the production workspace but if it doesnt exist then create it first
        - terraform workspace select production || terraform workspace new production
        - terraform plan
    rules:
        - if: '$CI_COMMIT_BRANCH == "production"'
    
Production Apply:
    stage: Production Apply
    script:
        - cd deploy/
        - export TF_VAR_ecr_image_api=$ECR_REPO:$CI_COMMIT_SHORT_SHA
        - terraform init
        # this runs after the production plan, so we know the workspace will already exist
        - terraform workspace select production
        - terraform apply -auto-approve
    rules:
        - if: '$CI_COMMIT_BRANCH == "production"'

Staging Destroy:
    stage: Destroy
    script: 
        - cd deploy/
        - terraform init
        - terraform workspace select staging
        - terraform destroy --auto-approve
    rules: 
        - if: '$CI_COMMIT_BRANCH =~ /^(master|production)$/'
          when: manual

Production Destroy:
    stage: Destroy
    script: 
        - cd deploy/
        - terraform init
        - terraform workspace select production
        - terraform destroy --auto-approve
    rules: 
        - if: '$CI_COMMIT_BRANCH == "production"'
          when: manual