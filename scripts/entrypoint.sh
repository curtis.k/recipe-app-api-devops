#!/bin/sh

set -e

# when the dockerfile is run (without docker compose) the database will be created already in RDS and the environment variables will be set with proper hostname already
python manage.py collectstatic --noinput
python manage.py wait_for_db
python manage.py migrate

# run on port 9000
# 4 workers per container... can also just run 1 worker for each docker container and run more docker containers
# run as the master service on the terminal
# enable multithreading
# starting poiint of app.wsgi (in the app folder)
uwsgi --socket :9000 --workers 4 --master --enable-threads --module app.wsgi