# Recipe App API DevOps Starting Point

Source code for my Udemy course Build a [Backend REST API with Python & Django - Advanced](http://udemy.com/django-python-advanced/).

The course teaches how to build a fully functioning REST API using:

 - Python
 - Django / Django-REST-Framework
 - Docker / Docker-Compose
 - Test Driven Development

## Getting started

To start project, run:

```
newgrp docker
```

To be able to run docker without sudo

Then: 

```
docker-compose up
```

- This will start the development Django runserver command with a postgres database
- The API will then be available at http://127.0.0.1:8000

To run the project with nginx as the proxy and test out a production deployment on your local machine:

If the proxy docker image has not been built yet, you will have to run the following command from the proxy directory

```
docker build .

```

- This will tag it with the "latest" tag so we can use it with our main docker compose file in the api-devops build
- Now from the main api/devops build, you can run 

```
docker-compose -f docker-compose-proxy.yml up
```

- Instead of using Django Runserver, this will use NGINX to serve the static files and pass along the app requests to uwsgi

To only run the tests and linitng, but not actually run the server, you can:

```
docker-compose run app sh -c "python manage.py test && flake8"
```

## Terraform 

Terraform is used to write infrastructure as code when you are using cloud deployents such as in AWS. The terraform commands can also be run through docker and has its own docker-compose file. 

- To first authenticate with AWS, run: `aws-vault exec curtis --duration=12h` then enter the MFA code. Note that you might need to close and reopen a terminal first
- Then to perform a terraform init, to initialize terraform, run: `docker-compose -f deploy/docker-compose.yml run --rm terraform init`
    - This initializes terraform before any other actions can be deployents
- To create a workspace run: `docker-compose -f deploy/docker-compose.yml run --rm terraform workspace new <name>`
- To use this workspace run: `docker-compose -f deploy/docker-compose.yml run --rm terraform workspace select <name>`
- To format the terraform code before excecuting, run: `docker-compose -f deploy/docker-compose.yml run --rm terraform fmt`
- To validate the terraform before excecuting, run: `docker-compose -f deploy/docker-compose.yml run --rm terraform validate`
- To see what terraform will create before actually creating the resources, run: `docker-compose -f deploy/docker-compose.yml run --rm terraform plan`
- To create/update the resources in the currently selected environment, run: `docker-compose -f deploy/docker-compose.yml run --rm terraform apply`
- To destroy the resources in the currently selected envirnoment, run: `docker-compose -f deploy/docker-compose.yml run --rm terraform destroy`

## Automatic Deployments for the API image

This project uses gitlab for automatically building our enviroment and deploying the code once certain actions are performed on the repository

Merge Request to Master:
- When a merge request is made from a development branch into master, the tests are run on the project to make sure everything passes before merging

Merge into Master:
- Once code is commited to the master branch, the tests are run again, and then the Staging Deploy is run using Terraform to deploy our Staging environment
- An optional Destroy job is available, to destroy the environment by manually executing the job

Merge Request to Production:
- The tests are run first to make sure the code is stable once a merge request is made to merge into a production branch

Merge into Production:
- Once code is commited/merged into production, the Staging build will be ran and pushed, as well as the Production build and push
- Again, optional manual tasks will be available to run on command for destroying both environments

## Automatic Deployments for the Proxy image

Gitlab pipelines/ automatic deployments are also used for the Proxy image

Push to Repo:
- Each time the code us pushed, it will build the docker image and save it to a shared artifact as data/image.tar

Commit to Master:
- When there is a commit into master, the image will be loaded from the latest artifact and tagged as a "dev" build then pushed to the Amazon ECR repo

Commit with "-release" at the end of the tag:
- When there is a commit tagged with -release at the end, it will take the latest build and push out two docker images to ECR. One will be with the "latest" tag and one will be with the git tag with "-release" stripped out

## Managing the app once it is deployed

We have created a bastion EC2 instance so we can manage our application once it is deployed to AWS

- First get the hostname of the Bastion instance
    - From the Gitlab pipeline, click the Staging Apply job from the most recent build and scroll down the bottom to view the console output. Copy the dns beside "bastion_host"
    - You can also get the hostname from AWS
- The default user is ec2-user, so to ssh into it, run `ssh ec2-user@<bastion_hostname>`
    - This will use the rsa keypair we added to AWS from our local machine
-  Once logged in, get the ECR login by: `$(aws ecr get-login --no-include-email --region us-east-1)`
- Now run the commands you need to use to manage the server, ie "createsuperuser":

```
docker run -it \
    -e DB_HOST=<DB_HOST> \
    -e DB_NAME=recipe \
    -e DB_USER=recipeapp \
    -e DB_PASS=<DB_PASS> \
    <ECR_REPO>:latest \
    sh -c "python manage.py wait_for_db && python manage.py createsuperuser"
```

Note you can get the HOST and PASS from Gitlab Variables or from the AWS ECS cluster. The ECR_REPO can be found from AWS ECR